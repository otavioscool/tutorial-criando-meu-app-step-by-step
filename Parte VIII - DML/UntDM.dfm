object DM: TDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 581
  Width = 852
  object fdcConexao: TFDConnection
    Params.Strings = (
      'Database=$(DB_LOCADORA_SQLITE)'
      'OpenMode=ReadWrite'
      'DriverID=SQLite')
    LoginPrompt = False
    Left = 80
    Top = 16
  end
  object qryTitulos: TFDQuery
    Connection = fdcConexao
    SQL.Strings = (
      'SELECT'
      '  ROWID,'
      '  ID_TITULO,'
      '  TITULO,'
      '  SUBTITULO,'
      '  ANO_LANCAMENTO,'
      '  FOTO'
      'FROM'
      '  TITULOS')
    Left = 80
    Top = 80
    object qryTitulosrowid: TLargeintField
      AutoGenerateValue = arDefault
      FieldName = 'rowid'
      Origin = 'rowid'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object qryTitulosID_TITULO: TIntegerField
      FieldName = 'ID_TITULO'
      Origin = 'ID_TITULO'
      Required = True
    end
    object qryTitulosTITULO: TStringField
      FieldName = 'TITULO'
      Origin = 'TITULO'
      Required = True
      Size = 30
    end
    object qryTitulosSUBTITULO: TStringField
      FieldName = 'SUBTITULO'
      Origin = 'SUBTITULO'
      Size = 50
    end
    object qryTitulosANO_LANCAMENTO: TIntegerField
      FieldName = 'ANO_LANCAMENTO'
      Origin = 'ANO_LANCAMENTO'
    end
    object qryTitulosFOTO: TBlobField
      FieldName = 'FOTO'
      Origin = 'FOTO'
    end
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'FMX'
    Left = 592
    Top = 8
  end
  object FDPhysSQLiteDriverLink1: TFDPhysSQLiteDriverLink
    Left = 592
    Top = 64
  end
  object qryAuxiliar1: TFDQuery
    Connection = fdcConexao
    Left = 464
    Top = 8
  end
end
